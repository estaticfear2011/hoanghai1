import 'bootstrap';
import './owl.carousel.min';
import AOS from 'aos';
import 'aos/dist/aos.css'

let welcomeSlides = $('.hero-area');
let thumbs = $('#thumbs');

welcomeSlides.owlCarousel({
    items: 1,
    margin: 0,
    loop: true,
    nav: true,
    navText: ['<span class="arrow-left"></span>', '<span class="arrow-right"></span>'],
    dots: false,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 1000
});

thumbs.on("click", ".item", function(e) {
    e.preventDefault();
    let number = $(this).index();
    welcomeSlides.data("owl.carousel").to(number, 300, true);
});

$('.button-wrap').click(function() {
    $(this).closest('.block').find('.content').slideToggle('slow');
    $(this).closest('.block').find('.price').slideToggle('slow');
});
$('.q-item').click(function(){
    $(this).closest('.q-a-row').find('.a-item').slideToggle('slow');
});

AOS.init();